package com.varus.lp.model;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name="Bonus")
@XmlRootElement
public class Bonus {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @XmlElement
    private Long id;
    @XmlElement
    int size;
    @XmlElement
    String article;

    public Bonus() {
    }

    public Bonus(int size, String article) {
        this.size = size;
        this.article = article;
    }

    @Override
    public String toString() {
        return "Bonus{" +
                "id=" + id +
                ", size=" + size +
                ", article='" + article + '\'' +
                '}';
    }

    public Long getId() {
        return id;
    }

    public int getSize() {
        return size;
    }

    public String getArticle() {
        return article;
    }
}
