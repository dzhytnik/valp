
package com.varus.lp;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;


@Service("testService")
public class TestServiceImpl implements TestService {
    public void hello() {
        System.out.println("TestService.hello");
    }

    @EventListener
    public void handleContextRefresh(ContextRefreshedEvent event) {
        System.out.println("TestService.handleContextRefresh");
    }

}