package com.varus.lp;

import com.varus.lp.model.Bonus;

import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService()
@SOAPBinding(style = SOAPBinding.Style.RPC, use=SOAPBinding.Use.LITERAL)
public interface BonusService {

    @WebMethod
    public String addBonus(Bonus p);

    @WebMethod
    @WebResult(name="bonus")
    public Bonus findByArticle(String article);

}
