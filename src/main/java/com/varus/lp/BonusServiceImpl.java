package com.varus.lp;

import com.varus.lp.model.Bonus;
import com.varus.lp.repository.BonusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jws.WebService;

@WebService(endpointInterface = "com.varus.lp.BonusService")
@Component("bonusService")
public class BonusServiceImpl implements BonusService{

    @Autowired
    BonusRepository bonusRepository;

    @Autowired
    TestService testService;

    @Override
    public String addBonus(Bonus p) {
        //return bonusRepository.save(p) == null;
        return "Result from real web service: id=" + p.getId() + ", size=" + p.getSize() + ", article=" + p.getArticle();
    }

    @Override
    public Bonus findByArticle(String article) {
        testService.hello();
        return bonusRepository.findByArticleAndSize(article, 100).get(0);
    }
}
