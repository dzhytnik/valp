package com.varus.lp.repository;

import com.varus.lp.model.Bonus;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("bonusRepository")
public interface BonusRepository extends CrudRepository<Bonus, Long> {

    List<Bonus> findByArticleAndSize(String article, int size);

    Bonus findById(long id);
}
