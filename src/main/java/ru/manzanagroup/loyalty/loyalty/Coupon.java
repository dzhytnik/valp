
package ru.manzanagroup.loyalty.loyalty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Coupon complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Coupon"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Number" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EmissionID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TypeID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ApplicabilityMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ApplicabilityCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Coupon", propOrder = {
    "number",
    "emissionID",
    "typeID",
    "applicabilityMessage",
    "applicabilityCode"
})
public class Coupon {

    @XmlElement(name = "Number")
    protected String number;
    @XmlElement(name = "EmissionID")
    protected String emissionID;
    @XmlElement(name = "TypeID")
    protected String typeID;
    @XmlElement(name = "ApplicabilityMessage")
    protected String applicabilityMessage;
    @XmlElement(name = "ApplicabilityCode")
    protected String applicabilityCode;

    /**
     * Gets the value of the number property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumber() {
        return number;
    }

    /**
     * Sets the value of the number property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumber(String value) {
        this.number = value;
    }

    /**
     * Gets the value of the emissionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmissionID() {
        return emissionID;
    }

    /**
     * Sets the value of the emissionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmissionID(String value) {
        this.emissionID = value;
    }

    /**
     * Gets the value of the typeID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypeID() {
        return typeID;
    }

    /**
     * Sets the value of the typeID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypeID(String value) {
        this.typeID = value;
    }

    /**
     * Gets the value of the applicabilityMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicabilityMessage() {
        return applicabilityMessage;
    }

    /**
     * Sets the value of the applicabilityMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicabilityMessage(String value) {
        this.applicabilityMessage = value;
    }

    /**
     * Gets the value of the applicabilityCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicabilityCode() {
        return applicabilityCode;
    }

    /**
     * Sets the value of the applicabilityCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicabilityCode(String value) {
        this.applicabilityCode = value;
    }

}
