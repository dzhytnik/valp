
package ru.manzanagroup.loyalty.loyalty;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for BalanceResponseBase complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BalanceResponseBase"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://loyalty.manzanagroup.ru/loyalty.xsd}ResponseBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Card" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}Card" minOccurs="0"/&gt;
 *         &lt;element name="ContactID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CardBalance" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="CardNormalBalance" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="CardStatusBalance" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="CardActiveBalance" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="CardNormalActiveBalance" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="CardStatusActiveBalance" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="CardSumm" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="CardSummDiscounted" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="CardDiscount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="CardQuantity" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="ContactPresence" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}ContactPresence_Type" minOccurs="0"/&gt;
 *         &lt;element name="CardType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CardStatus" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="CardCollaborationType" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="CardChargeType" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="CardChargedBonus" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="CardWriteoffBonus" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="CardChargedMoney" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="CardWriteoffMoney" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="CardMoneyBalance" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="FullMoneyBalance" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="CashierMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CashierMessage2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ChequeMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ChequeMessage2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PersonalMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PersonalMessage2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MiddleName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FullName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BirthDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="Age" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="Phone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BalanceResponseBase", propOrder = {
    "card",
    "contactID",
    "cardBalance",
    "cardNormalBalance",
    "cardStatusBalance",
    "cardActiveBalance",
    "cardNormalActiveBalance",
    "cardStatusActiveBalance",
    "cardSumm",
    "cardSummDiscounted",
    "cardDiscount",
    "cardQuantity",
    "contactPresence",
    "cardType",
    "cardStatus",
    "cardCollaborationType",
    "cardChargeType",
    "cardChargedBonus",
    "cardWriteoffBonus",
    "cardChargedMoney",
    "cardWriteoffMoney",
    "cardMoneyBalance",
    "fullMoneyBalance",
    "cashierMessage",
    "cashierMessage2",
    "chequeMessage",
    "chequeMessage2",
    "personalMessage",
    "personalMessage2",
    "firstName",
    "lastName",
    "middleName",
    "fullName",
    "birthDate",
    "age",
    "phone",
    "email"
})
@XmlSeeAlso({
    ChequeResponseBase.class
})
public class BalanceResponseBase
    extends ResponseBase
{

    @XmlElement(name = "Card")
    protected Card card;
    @XmlElement(name = "ContactID")
    protected String contactID;
    @XmlElement(name = "CardBalance")
    protected BigDecimal cardBalance;
    @XmlElement(name = "CardNormalBalance")
    protected BigDecimal cardNormalBalance;
    @XmlElement(name = "CardStatusBalance")
    protected BigDecimal cardStatusBalance;
    @XmlElement(name = "CardActiveBalance")
    protected BigDecimal cardActiveBalance;
    @XmlElement(name = "CardNormalActiveBalance")
    protected BigDecimal cardNormalActiveBalance;
    @XmlElement(name = "CardStatusActiveBalance")
    protected BigDecimal cardStatusActiveBalance;
    @XmlElement(name = "CardSumm")
    protected BigDecimal cardSumm;
    @XmlElement(name = "CardSummDiscounted")
    protected BigDecimal cardSummDiscounted;
    @XmlElement(name = "CardDiscount")
    protected BigDecimal cardDiscount;
    @XmlElement(name = "CardQuantity")
    protected BigInteger cardQuantity;
    @XmlElement(name = "ContactPresence")
    protected String contactPresence;
    @XmlElement(name = "CardType")
    protected String cardType;
    @XmlElement(name = "CardStatus")
    protected BigInteger cardStatus;
    @XmlElement(name = "CardCollaborationType")
    protected BigInteger cardCollaborationType;
    @XmlElement(name = "CardChargeType")
    protected BigInteger cardChargeType;
    @XmlElement(name = "CardChargedBonus")
    protected BigDecimal cardChargedBonus;
    @XmlElement(name = "CardWriteoffBonus")
    protected BigDecimal cardWriteoffBonus;
    @XmlElement(name = "CardChargedMoney")
    protected BigDecimal cardChargedMoney;
    @XmlElement(name = "CardWriteoffMoney")
    protected BigDecimal cardWriteoffMoney;
    @XmlElement(name = "CardMoneyBalance")
    protected BigDecimal cardMoneyBalance;
    @XmlElement(name = "FullMoneyBalance")
    protected BigDecimal fullMoneyBalance;
    @XmlElement(name = "CashierMessage")
    protected String cashierMessage;
    @XmlElement(name = "CashierMessage2")
    protected String cashierMessage2;
    @XmlElement(name = "ChequeMessage")
    protected String chequeMessage;
    @XmlElement(name = "ChequeMessage2")
    protected String chequeMessage2;
    @XmlElement(name = "PersonalMessage")
    protected String personalMessage;
    @XmlElement(name = "PersonalMessage2")
    protected String personalMessage2;
    @XmlElement(name = "FirstName")
    protected String firstName;
    @XmlElement(name = "LastName")
    protected String lastName;
    @XmlElement(name = "MiddleName")
    protected String middleName;
    @XmlElement(name = "FullName")
    protected String fullName;
    @XmlElement(name = "BirthDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar birthDate;
    @XmlElement(name = "Age")
    protected BigInteger age;
    @XmlElement(name = "Phone")
    protected String phone;
    @XmlElement(name = "Email")
    protected String email;

    /**
     * Gets the value of the card property.
     * 
     * @return
     *     possible object is
     *     {@link Card }
     *     
     */
    public Card getCard() {
        return card;
    }

    /**
     * Sets the value of the card property.
     * 
     * @param value
     *     allowed object is
     *     {@link Card }
     *     
     */
    public void setCard(Card value) {
        this.card = value;
    }

    /**
     * Gets the value of the contactID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactID() {
        return contactID;
    }

    /**
     * Sets the value of the contactID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactID(String value) {
        this.contactID = value;
    }

    /**
     * Gets the value of the cardBalance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCardBalance() {
        return cardBalance;
    }

    /**
     * Sets the value of the cardBalance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCardBalance(BigDecimal value) {
        this.cardBalance = value;
    }

    /**
     * Gets the value of the cardNormalBalance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCardNormalBalance() {
        return cardNormalBalance;
    }

    /**
     * Sets the value of the cardNormalBalance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCardNormalBalance(BigDecimal value) {
        this.cardNormalBalance = value;
    }

    /**
     * Gets the value of the cardStatusBalance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCardStatusBalance() {
        return cardStatusBalance;
    }

    /**
     * Sets the value of the cardStatusBalance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCardStatusBalance(BigDecimal value) {
        this.cardStatusBalance = value;
    }

    /**
     * Gets the value of the cardActiveBalance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCardActiveBalance() {
        return cardActiveBalance;
    }

    /**
     * Sets the value of the cardActiveBalance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCardActiveBalance(BigDecimal value) {
        this.cardActiveBalance = value;
    }

    /**
     * Gets the value of the cardNormalActiveBalance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCardNormalActiveBalance() {
        return cardNormalActiveBalance;
    }

    /**
     * Sets the value of the cardNormalActiveBalance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCardNormalActiveBalance(BigDecimal value) {
        this.cardNormalActiveBalance = value;
    }

    /**
     * Gets the value of the cardStatusActiveBalance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCardStatusActiveBalance() {
        return cardStatusActiveBalance;
    }

    /**
     * Sets the value of the cardStatusActiveBalance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCardStatusActiveBalance(BigDecimal value) {
        this.cardStatusActiveBalance = value;
    }

    /**
     * Gets the value of the cardSumm property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCardSumm() {
        return cardSumm;
    }

    /**
     * Sets the value of the cardSumm property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCardSumm(BigDecimal value) {
        this.cardSumm = value;
    }

    /**
     * Gets the value of the cardSummDiscounted property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCardSummDiscounted() {
        return cardSummDiscounted;
    }

    /**
     * Sets the value of the cardSummDiscounted property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCardSummDiscounted(BigDecimal value) {
        this.cardSummDiscounted = value;
    }

    /**
     * Gets the value of the cardDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCardDiscount() {
        return cardDiscount;
    }

    /**
     * Sets the value of the cardDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCardDiscount(BigDecimal value) {
        this.cardDiscount = value;
    }

    /**
     * Gets the value of the cardQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCardQuantity() {
        return cardQuantity;
    }

    /**
     * Sets the value of the cardQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCardQuantity(BigInteger value) {
        this.cardQuantity = value;
    }

    /**
     * Gets the value of the contactPresence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactPresence() {
        return contactPresence;
    }

    /**
     * Sets the value of the contactPresence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactPresence(String value) {
        this.contactPresence = value;
    }

    /**
     * Gets the value of the cardType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardType() {
        return cardType;
    }

    /**
     * Sets the value of the cardType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardType(String value) {
        this.cardType = value;
    }

    /**
     * Gets the value of the cardStatus property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCardStatus() {
        return cardStatus;
    }

    /**
     * Sets the value of the cardStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCardStatus(BigInteger value) {
        this.cardStatus = value;
    }

    /**
     * Gets the value of the cardCollaborationType property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCardCollaborationType() {
        return cardCollaborationType;
    }

    /**
     * Sets the value of the cardCollaborationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCardCollaborationType(BigInteger value) {
        this.cardCollaborationType = value;
    }

    /**
     * Gets the value of the cardChargeType property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCardChargeType() {
        return cardChargeType;
    }

    /**
     * Sets the value of the cardChargeType property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCardChargeType(BigInteger value) {
        this.cardChargeType = value;
    }

    /**
     * Gets the value of the cardChargedBonus property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCardChargedBonus() {
        return cardChargedBonus;
    }

    /**
     * Sets the value of the cardChargedBonus property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCardChargedBonus(BigDecimal value) {
        this.cardChargedBonus = value;
    }

    /**
     * Gets the value of the cardWriteoffBonus property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCardWriteoffBonus() {
        return cardWriteoffBonus;
    }

    /**
     * Sets the value of the cardWriteoffBonus property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCardWriteoffBonus(BigDecimal value) {
        this.cardWriteoffBonus = value;
    }

    /**
     * Gets the value of the cardChargedMoney property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCardChargedMoney() {
        return cardChargedMoney;
    }

    /**
     * Sets the value of the cardChargedMoney property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCardChargedMoney(BigDecimal value) {
        this.cardChargedMoney = value;
    }

    /**
     * Gets the value of the cardWriteoffMoney property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCardWriteoffMoney() {
        return cardWriteoffMoney;
    }

    /**
     * Sets the value of the cardWriteoffMoney property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCardWriteoffMoney(BigDecimal value) {
        this.cardWriteoffMoney = value;
    }

    /**
     * Gets the value of the cardMoneyBalance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCardMoneyBalance() {
        return cardMoneyBalance;
    }

    /**
     * Sets the value of the cardMoneyBalance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCardMoneyBalance(BigDecimal value) {
        this.cardMoneyBalance = value;
    }

    /**
     * Gets the value of the fullMoneyBalance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFullMoneyBalance() {
        return fullMoneyBalance;
    }

    /**
     * Sets the value of the fullMoneyBalance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFullMoneyBalance(BigDecimal value) {
        this.fullMoneyBalance = value;
    }

    /**
     * Gets the value of the cashierMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCashierMessage() {
        return cashierMessage;
    }

    /**
     * Sets the value of the cashierMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCashierMessage(String value) {
        this.cashierMessage = value;
    }

    /**
     * Gets the value of the cashierMessage2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCashierMessage2() {
        return cashierMessage2;
    }

    /**
     * Sets the value of the cashierMessage2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCashierMessage2(String value) {
        this.cashierMessage2 = value;
    }

    /**
     * Gets the value of the chequeMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChequeMessage() {
        return chequeMessage;
    }

    /**
     * Sets the value of the chequeMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChequeMessage(String value) {
        this.chequeMessage = value;
    }

    /**
     * Gets the value of the chequeMessage2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChequeMessage2() {
        return chequeMessage2;
    }

    /**
     * Sets the value of the chequeMessage2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChequeMessage2(String value) {
        this.chequeMessage2 = value;
    }

    /**
     * Gets the value of the personalMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPersonalMessage() {
        return personalMessage;
    }

    /**
     * Sets the value of the personalMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersonalMessage(String value) {
        this.personalMessage = value;
    }

    /**
     * Gets the value of the personalMessage2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPersonalMessage2() {
        return personalMessage2;
    }

    /**
     * Sets the value of the personalMessage2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersonalMessage2(String value) {
        this.personalMessage2 = value;
    }

    /**
     * Gets the value of the firstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the value of the firstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Gets the value of the lastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the value of the lastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

    /**
     * Gets the value of the middleName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Sets the value of the middleName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMiddleName(String value) {
        this.middleName = value;
    }

    /**
     * Gets the value of the fullName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * Sets the value of the fullName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFullName(String value) {
        this.fullName = value;
    }

    /**
     * Gets the value of the birthDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBirthDate() {
        return birthDate;
    }

    /**
     * Sets the value of the birthDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBirthDate(XMLGregorianCalendar value) {
        this.birthDate = value;
    }

    /**
     * Gets the value of the age property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAge() {
        return age;
    }

    /**
     * Sets the value of the age property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAge(BigInteger value) {
        this.age = value;
    }

    /**
     * Gets the value of the phone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Sets the value of the phone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone(String value) {
        this.phone = value;
    }

    /**
     * Gets the value of the email property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

}
