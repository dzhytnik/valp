
package ru.manzanagroup.loyalty.loyalty;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OfferElement complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OfferElement"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="OfferElementName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="OfferElementID" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="Article" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ArticleGroup" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ArticleExtGroup" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Manufacturer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Quantity" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OfferElement", propOrder = {
    "offerElementName",
    "offerElementID",
    "article",
    "articleGroup",
    "articleExtGroup",
    "manufacturer",
    "quantity"
})
public class OfferElement {

    @XmlElement(name = "OfferElementName")
    protected String offerElementName;
    @XmlElement(name = "OfferElementID")
    protected BigInteger offerElementID;
    @XmlElement(name = "Article")
    protected String article;
    @XmlElement(name = "ArticleGroup")
    protected String articleGroup;
    @XmlElement(name = "ArticleExtGroup")
    protected String articleExtGroup;
    @XmlElement(name = "Manufacturer")
    protected String manufacturer;
    @XmlElement(name = "Quantity", required = true)
    protected BigDecimal quantity;

    /**
     * Gets the value of the offerElementName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfferElementName() {
        return offerElementName;
    }

    /**
     * Sets the value of the offerElementName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfferElementName(String value) {
        this.offerElementName = value;
    }

    /**
     * Gets the value of the offerElementID property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getOfferElementID() {
        return offerElementID;
    }

    /**
     * Sets the value of the offerElementID property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setOfferElementID(BigInteger value) {
        this.offerElementID = value;
    }

    /**
     * Gets the value of the article property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArticle() {
        return article;
    }

    /**
     * Sets the value of the article property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArticle(String value) {
        this.article = value;
    }

    /**
     * Gets the value of the articleGroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArticleGroup() {
        return articleGroup;
    }

    /**
     * Sets the value of the articleGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArticleGroup(String value) {
        this.articleGroup = value;
    }

    /**
     * Gets the value of the articleExtGroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArticleExtGroup() {
        return articleExtGroup;
    }

    /**
     * Sets the value of the articleExtGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArticleExtGroup(String value) {
        this.articleExtGroup = value;
    }

    /**
     * Gets the value of the manufacturer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getManufacturer() {
        return manufacturer;
    }

    /**
     * Sets the value of the manufacturer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setManufacturer(String value) {
        this.manufacturer = value;
    }

    /**
     * Gets the value of the quantity property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getQuantity() {
        return quantity;
    }

    /**
     * Sets the value of the quantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setQuantity(BigDecimal value) {
        this.quantity = value;
    }

}
