
package ru.manzanagroup.loyalty.loyalty;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.manzanagroup.loyalty.loyalty package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ProcessRequestRequestBalanceRequest_QNAME = new QName("http://loyalty.manzanagroup.ru/loyalty.xsd", "BalanceRequest");
    private final static QName _ProcessRequestRequestBonusRequest_QNAME = new QName("http://loyalty.manzanagroup.ru/loyalty.xsd", "BonusRequest");
    private final static QName _ProcessRequestRequestCardManagementRequest_QNAME = new QName("http://loyalty.manzanagroup.ru/loyalty.xsd", "CardManagementRequest");
    private final static QName _ProcessRequestRequestCardRequest_QNAME = new QName("http://loyalty.manzanagroup.ru/loyalty.xsd", "CardRequest");
    private final static QName _ProcessRequestRequestChequeRequest_QNAME = new QName("http://loyalty.manzanagroup.ru/loyalty.xsd", "ChequeRequest");
    private final static QName _ProcessRequestRequestMoneyRequest_QNAME = new QName("http://loyalty.manzanagroup.ru/loyalty.xsd", "MoneyRequest");
    private final static QName _ProcessRequestRequestMoneyRollback_QNAME = new QName("http://loyalty.manzanagroup.ru/loyalty.xsd", "MoneyRollback");
    private final static QName _ProcessRequestRequestOfferRequest_QNAME = new QName("http://loyalty.manzanagroup.ru/loyalty.xsd", "OfferRequest");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.manzanagroup.loyalty.loyalty
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ProcessRequest }
     * 
     */
    public ProcessRequest createProcessRequest() {
        return new ProcessRequest();
    }

    /**
     * Create an instance of {@link ProcessRequestResponse }
     * 
     */
    public ProcessRequestResponse createProcessRequestResponse() {
        return new ProcessRequestResponse();
    }

    /**
     * Create an instance of {@link ProcessRequest.Request }
     * 
     */
    public ProcessRequest.Request createProcessRequestRequest() {
        return new ProcessRequest.Request();
    }

    /**
     * Create an instance of {@link ProcessRequestResponse.ProcessRequestResult }
     * 
     */
    public ProcessRequestResponse.ProcessRequestResult createProcessRequestResponseProcessRequestResult() {
        return new ProcessRequestResponse.ProcessRequestResult();
    }

    /**
     * Create an instance of {@link BalanceRequestBase }
     * 
     */
    public BalanceRequestBase createBalanceRequestBase() {
        return new BalanceRequestBase();
    }

    /**
     * Create an instance of {@link RequestBase }
     * 
     */
    public RequestBase createRequestBase() {
        return new RequestBase();
    }

    /**
     * Create an instance of {@link Card }
     * 
     */
    public Card createCard() {
        return new Card();
    }

    /**
     * Create an instance of {@link MobilePhone }
     * 
     */
    public MobilePhone createMobilePhone() {
        return new MobilePhone();
    }

    /**
     * Create an instance of {@link ChequeReference }
     * 
     */
    public ChequeReference createChequeReference() {
        return new ChequeReference();
    }

    /**
     * Create an instance of {@link TransactionReference }
     * 
     */
    public TransactionReference createTransactionReference() {
        return new TransactionReference();
    }

    /**
     * Create an instance of {@link Cheque }
     * 
     */
    public Cheque createCheque() {
        return new Cheque();
    }

    /**
     * Create an instance of {@link Payment }
     * 
     */
    public Payment createPayment() {
        return new Payment();
    }

    /**
     * Create an instance of {@link Coupons }
     * 
     */
    public Coupons createCoupons() {
        return new Coupons();
    }

    /**
     * Create an instance of {@link Coupon }
     * 
     */
    public Coupon createCoupon() {
        return new Coupon();
    }

    /**
     * Create an instance of {@link ExtendedAttributes }
     * 
     */
    public ExtendedAttributes createExtendedAttributes() {
        return new ExtendedAttributes();
    }

    /**
     * Create an instance of {@link ChequeItem }
     * 
     */
    public ChequeItem createChequeItem() {
        return new ChequeItem();
    }

    /**
     * Create an instance of {@link Money }
     * 
     */
    public Money createMoney() {
        return new Money();
    }

    /**
     * Create an instance of {@link MoneyRollbackBase }
     * 
     */
    public MoneyRollbackBase createMoneyRollbackBase() {
        return new MoneyRollbackBase();
    }

    /**
     * Create an instance of {@link MoneyReference }
     * 
     */
    public MoneyReference createMoneyReference() {
        return new MoneyReference();
    }

    /**
     * Create an instance of {@link CardRequestBase }
     * 
     */
    public CardRequestBase createCardRequestBase() {
        return new CardRequestBase();
    }

    /**
     * Create an instance of {@link CardManagementRequestBase }
     * 
     */
    public CardManagementRequestBase createCardManagementRequestBase() {
        return new CardManagementRequestBase();
    }

    /**
     * Create an instance of {@link BonusRequestBase }
     * 
     */
    public BonusRequestBase createBonusRequestBase() {
        return new BonusRequestBase();
    }

    /**
     * Create an instance of {@link BalanceResponseBase }
     * 
     */
    public BalanceResponseBase createBalanceResponseBase() {
        return new BalanceResponseBase();
    }

    /**
     * Create an instance of {@link ResponseBase }
     * 
     */
    public ResponseBase createResponseBase() {
        return new ResponseBase();
    }

    /**
     * Create an instance of {@link PersonalOfferType }
     * 
     */
    public PersonalOfferType createPersonalOfferType() {
        return new PersonalOfferType();
    }

    /**
     * Create an instance of {@link OfferResponseBase }
     * 
     */
    public OfferResponseBase createOfferResponseBase() {
        return new OfferResponseBase();
    }

    /**
     * Create an instance of {@link Offer }
     * 
     */
    public Offer createOffer() {
        return new Offer();
    }

    /**
     * Create an instance of {@link OfferElement }
     * 
     */
    public OfferElement createOfferElement() {
        return new OfferElement();
    }

    /**
     * Create an instance of {@link CardResponseBase }
     * 
     */
    public CardResponseBase createCardResponseBase() {
        return new CardResponseBase();
    }

    /**
     * Create an instance of {@link MoneyResponseBase }
     * 
     */
    public MoneyResponseBase createMoneyResponseBase() {
        return new MoneyResponseBase();
    }

    /**
     * Create an instance of {@link CardManagementResponseBase }
     * 
     */
    public CardManagementResponseBase createCardManagementResponseBase() {
        return new CardManagementResponseBase();
    }

    /**
     * Create an instance of {@link ChequeResponseBase }
     * 
     */
    public ChequeResponseBase createChequeResponseBase() {
        return new ChequeResponseBase();
    }

    /**
     * Create an instance of {@link InstantCoupons }
     * 
     */
    public InstantCoupons createInstantCoupons() {
        return new InstantCoupons();
    }

    /**
     * Create an instance of {@link InstantCoupon }
     * 
     */
    public InstantCoupon createInstantCoupon() {
        return new InstantCoupon();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BalanceRequestBase }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link BalanceRequestBase }{@code >}
     */
    @XmlElementDecl(namespace = "http://loyalty.manzanagroup.ru/loyalty.xsd", name = "BalanceRequest", scope = ProcessRequest.Request.class)
    public JAXBElement<BalanceRequestBase> createProcessRequestRequestBalanceRequest(BalanceRequestBase value) {
        return new JAXBElement<BalanceRequestBase>(_ProcessRequestRequestBalanceRequest_QNAME, BalanceRequestBase.class, ProcessRequest.Request.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BonusRequestBase }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link BonusRequestBase }{@code >}
     */
    @XmlElementDecl(namespace = "http://loyalty.manzanagroup.ru/loyalty.xsd", name = "BonusRequest", scope = ProcessRequest.Request.class)
    public JAXBElement<BonusRequestBase> createProcessRequestRequestBonusRequest(BonusRequestBase value) {
        return new JAXBElement<BonusRequestBase>(_ProcessRequestRequestBonusRequest_QNAME, BonusRequestBase.class, ProcessRequest.Request.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardManagementRequestBase }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CardManagementRequestBase }{@code >}
     */
    @XmlElementDecl(namespace = "http://loyalty.manzanagroup.ru/loyalty.xsd", name = "CardManagementRequest", scope = ProcessRequest.Request.class)
    public JAXBElement<CardManagementRequestBase> createProcessRequestRequestCardManagementRequest(CardManagementRequestBase value) {
        return new JAXBElement<CardManagementRequestBase>(_ProcessRequestRequestCardManagementRequest_QNAME, CardManagementRequestBase.class, ProcessRequest.Request.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardRequestBase }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CardRequestBase }{@code >}
     */
    @XmlElementDecl(namespace = "http://loyalty.manzanagroup.ru/loyalty.xsd", name = "CardRequest", scope = ProcessRequest.Request.class)
    public JAXBElement<CardRequestBase> createProcessRequestRequestCardRequest(CardRequestBase value) {
        return new JAXBElement<CardRequestBase>(_ProcessRequestRequestCardRequest_QNAME, CardRequestBase.class, ProcessRequest.Request.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Cheque }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Cheque }{@code >}
     */
    @XmlElementDecl(namespace = "http://loyalty.manzanagroup.ru/loyalty.xsd", name = "ChequeRequest", scope = ProcessRequest.Request.class)
    public JAXBElement<Cheque> createProcessRequestRequestChequeRequest(Cheque value) {
        return new JAXBElement<Cheque>(_ProcessRequestRequestChequeRequest_QNAME, Cheque.class, ProcessRequest.Request.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Money }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Money }{@code >}
     */
    @XmlElementDecl(namespace = "http://loyalty.manzanagroup.ru/loyalty.xsd", name = "MoneyRequest", scope = ProcessRequest.Request.class)
    public JAXBElement<Money> createProcessRequestRequestMoneyRequest(Money value) {
        return new JAXBElement<Money>(_ProcessRequestRequestMoneyRequest_QNAME, Money.class, ProcessRequest.Request.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MoneyRollbackBase }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link MoneyRollbackBase }{@code >}
     */
    @XmlElementDecl(namespace = "http://loyalty.manzanagroup.ru/loyalty.xsd", name = "MoneyRollback", scope = ProcessRequest.Request.class)
    public JAXBElement<MoneyRollbackBase> createProcessRequestRequestMoneyRollback(MoneyRollbackBase value) {
        return new JAXBElement<MoneyRollbackBase>(_ProcessRequestRequestMoneyRollback_QNAME, MoneyRollbackBase.class, ProcessRequest.Request.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Cheque }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Cheque }{@code >}
     */
    @XmlElementDecl(namespace = "http://loyalty.manzanagroup.ru/loyalty.xsd", name = "OfferRequest", scope = ProcessRequest.Request.class)
    public JAXBElement<Cheque> createProcessRequestRequestOfferRequest(Cheque value) {
        return new JAXBElement<Cheque>(_ProcessRequestRequestOfferRequest_QNAME, Cheque.class, ProcessRequest.Request.class, value);
    }

}
