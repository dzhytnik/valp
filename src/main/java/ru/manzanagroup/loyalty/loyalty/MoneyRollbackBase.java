
package ru.manzanagroup.loyalty.loyalty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MoneyRollbackBase complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MoneyRollbackBase"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://loyalty.manzanagroup.ru/loyalty.xsd}RequestBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Number" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MoneyReference" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}MoneyReference" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MoneyRollbackBase", propOrder = {
    "number",
    "moneyReference"
})
public class MoneyRollbackBase
    extends RequestBase
{

    @XmlElement(name = "Number")
    protected String number;
    @XmlElement(name = "MoneyReference")
    protected MoneyReference moneyReference;

    /**
     * Gets the value of the number property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumber() {
        return number;
    }

    /**
     * Sets the value of the number property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumber(String value) {
        this.number = value;
    }

    /**
     * Gets the value of the moneyReference property.
     * 
     * @return
     *     possible object is
     *     {@link MoneyReference }
     *     
     */
    public MoneyReference getMoneyReference() {
        return moneyReference;
    }

    /**
     * Sets the value of the moneyReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link MoneyReference }
     *     
     */
    public void setMoneyReference(MoneyReference value) {
        this.moneyReference = value;
    }

}
