
package ru.manzanagroup.loyalty.loyalty;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Card complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Card"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="CardNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *           &lt;element name="Track2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *           &lt;element name="Tracks" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="BonusType" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}BonusType_Type" minOccurs="0"/&gt;
 *         &lt;element name="Discount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="Status" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}Status_Type" minOccurs="0"/&gt;
 *         &lt;element name="CollaborationType" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}CollaborationType_Type" minOccurs="0"/&gt;
 *         &lt;element name="CardTypeID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Card", propOrder = {
    "cardNumber",
    "track2",
    "tracks",
    "bonusType",
    "discount",
    "status",
    "collaborationType",
    "cardTypeID"
})
public class Card {

    @XmlElement(name = "CardNumber")
    protected String cardNumber;
    @XmlElement(name = "Track2")
    protected String track2;
    @XmlElement(name = "Tracks")
    protected String tracks;
    @XmlElement(name = "BonusType")
    protected String bonusType;
    @XmlElement(name = "Discount")
    protected BigDecimal discount;
    @XmlElement(name = "Status")
    protected String status;
    @XmlElement(name = "CollaborationType")
    protected String collaborationType;
    @XmlElement(name = "CardTypeID")
    protected String cardTypeID;

    /**
     * Gets the value of the cardNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardNumber() {
        return cardNumber;
    }

    /**
     * Sets the value of the cardNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardNumber(String value) {
        this.cardNumber = value;
    }

    /**
     * Gets the value of the track2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrack2() {
        return track2;
    }

    /**
     * Sets the value of the track2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrack2(String value) {
        this.track2 = value;
    }

    /**
     * Gets the value of the tracks property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTracks() {
        return tracks;
    }

    /**
     * Sets the value of the tracks property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTracks(String value) {
        this.tracks = value;
    }

    /**
     * Gets the value of the bonusType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBonusType() {
        return bonusType;
    }

    /**
     * Sets the value of the bonusType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBonusType(String value) {
        this.bonusType = value;
    }

    /**
     * Gets the value of the discount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDiscount() {
        return discount;
    }

    /**
     * Sets the value of the discount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDiscount(BigDecimal value) {
        this.discount = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the collaborationType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCollaborationType() {
        return collaborationType;
    }

    /**
     * Sets the value of the collaborationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCollaborationType(String value) {
        this.collaborationType = value;
    }

    /**
     * Gets the value of the cardTypeID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardTypeID() {
        return cardTypeID;
    }

    /**
     * Sets the value of the cardTypeID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardTypeID(String value) {
        this.cardTypeID = value;
    }

}
