
package ru.manzanagroup.loyalty.loyalty;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ProcessRequestResult" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;choice maxOccurs="unbounded" minOccurs="0"&gt;
 *                     &lt;element name="BalanceResponse" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}BalanceResponseBase" minOccurs="0"/&gt;
 *                     &lt;element name="BonusResponse" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}ResponseBase" minOccurs="0"/&gt;
 *                     &lt;element name="CardManagementResponse" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}CardManagementResponseBase" minOccurs="0"/&gt;
 *                     &lt;element name="CardResponse" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}CardResponseBase" minOccurs="0"/&gt;
 *                     &lt;element name="ChequeResponse" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}ChequeResponseBase" minOccurs="0"/&gt;
 *                     &lt;element name="MoneyResponse" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}MoneyResponseBase" minOccurs="0"/&gt;
 *                     &lt;element name="OfferResponse" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}OfferResponseBase" minOccurs="0"/&gt;
 *                   &lt;/choice&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "processRequestResult"
})
@XmlRootElement(name = "ProcessRequestResponse")
public class ProcessRequestResponse {

    @XmlElement(name = "ProcessRequestResult")
    protected ProcessRequestResponse.ProcessRequestResult processRequestResult;

    /**
     * Gets the value of the processRequestResult property.
     * 
     * @return
     *     possible object is
     *     {@link ProcessRequestResponse.ProcessRequestResult }
     *     
     */
    public ProcessRequestResponse.ProcessRequestResult getProcessRequestResult() {
        return processRequestResult;
    }

    /**
     * Sets the value of the processRequestResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessRequestResponse.ProcessRequestResult }
     *     
     */
    public void setProcessRequestResult(ProcessRequestResponse.ProcessRequestResult value) {
        this.processRequestResult = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;choice maxOccurs="unbounded" minOccurs="0"&gt;
     *           &lt;element name="BalanceResponse" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}BalanceResponseBase" minOccurs="0"/&gt;
     *           &lt;element name="BonusResponse" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}ResponseBase" minOccurs="0"/&gt;
     *           &lt;element name="CardManagementResponse" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}CardManagementResponseBase" minOccurs="0"/&gt;
     *           &lt;element name="CardResponse" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}CardResponseBase" minOccurs="0"/&gt;
     *           &lt;element name="ChequeResponse" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}ChequeResponseBase" minOccurs="0"/&gt;
     *           &lt;element name="MoneyResponse" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}MoneyResponseBase" minOccurs="0"/&gt;
     *           &lt;element name="OfferResponse" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}OfferResponseBase" minOccurs="0"/&gt;
     *         &lt;/choice&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "balanceResponseOrBonusResponseOrCardManagementResponse"
    })
    public static class ProcessRequestResult {

        @XmlElements({
            @XmlElement(name = "BalanceResponse", type = BalanceResponseBase.class),
            @XmlElement(name = "BonusResponse"),
            @XmlElement(name = "CardManagementResponse", type = CardManagementResponseBase.class),
            @XmlElement(name = "CardResponse", type = CardResponseBase.class),
            @XmlElement(name = "ChequeResponse", type = ChequeResponseBase.class),
            @XmlElement(name = "MoneyResponse", type = MoneyResponseBase.class),
            @XmlElement(name = "OfferResponse", type = OfferResponseBase.class)
        })
        protected List<ResponseBase> balanceResponseOrBonusResponseOrCardManagementResponse;

        /**
         * Gets the value of the balanceResponseOrBonusResponseOrCardManagementResponse property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the balanceResponseOrBonusResponseOrCardManagementResponse property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getBalanceResponseOrBonusResponseOrCardManagementResponse().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BalanceResponseBase }
         * {@link ResponseBase }
         * {@link CardManagementResponseBase }
         * {@link CardResponseBase }
         * {@link ChequeResponseBase }
         * {@link MoneyResponseBase }
         * {@link OfferResponseBase }
         * 
         * 
         */
        public List<ResponseBase> getBalanceResponseOrBonusResponseOrCardManagementResponse() {
            if (balanceResponseOrBonusResponseOrCardManagementResponse == null) {
                balanceResponseOrBonusResponseOrCardManagementResponse = new ArrayList<ResponseBase>();
            }
            return this.balanceResponseOrBonusResponseOrCardManagementResponse;
        }

        public void setBalanceResponseOrBonusResponseOrCardManagementResponse(List<ResponseBase> value) {
            this.balanceResponseOrBonusResponseOrCardManagementResponse = null;
            List<ResponseBase> draftl = this.getBalanceResponseOrBonusResponseOrCardManagementResponse();
            draftl.addAll(value);
        }

    }

}
