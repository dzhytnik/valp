
package ru.manzanagroup.loyalty.loyalty;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ChequeItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ChequeItem"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PositionNumber" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/&gt;
 *         &lt;element name="Article" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ArticleName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Price" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="Quantity" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="Summ" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="Discount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="SummDiscounted" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="AvailablePayment" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="ChargedBonus" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="ChargedStatusBonus" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="WriteoffBonus" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="WriteoffStatusBonus" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="ActiveChargedBonus" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="ActiveChargedStatusBonus" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="ExtendedAttribute" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}ExtendedAttributes" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Banned" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChequeItem", propOrder = {
    "positionNumber",
    "article",
    "articleName",
    "price",
    "quantity",
    "summ",
    "discount",
    "summDiscounted",
    "availablePayment",
    "chargedBonus",
    "chargedStatusBonus",
    "writeoffBonus",
    "writeoffStatusBonus",
    "activeChargedBonus",
    "activeChargedStatusBonus",
    "extendedAttribute",
    "banned"
})
public class ChequeItem {

    @XmlElement(name = "PositionNumber")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger positionNumber;
    @XmlElement(name = "Article")
    protected String article;
    @XmlElement(name = "ArticleName")
    protected String articleName;
    @XmlElement(name = "Price")
    protected BigDecimal price;
    @XmlElement(name = "Quantity", required = true)
    protected BigDecimal quantity;
    @XmlElement(name = "Summ")
    protected BigDecimal summ;
    @XmlElement(name = "Discount")
    protected BigDecimal discount;
    @XmlElement(name = "SummDiscounted")
    protected BigDecimal summDiscounted;
    @XmlElement(name = "AvailablePayment")
    protected BigDecimal availablePayment;
    @XmlElement(name = "ChargedBonus")
    protected BigDecimal chargedBonus;
    @XmlElement(name = "ChargedStatusBonus")
    protected BigDecimal chargedStatusBonus;
    @XmlElement(name = "WriteoffBonus")
    protected BigDecimal writeoffBonus;
    @XmlElement(name = "WriteoffStatusBonus")
    protected BigDecimal writeoffStatusBonus;
    @XmlElement(name = "ActiveChargedBonus")
    protected BigDecimal activeChargedBonus;
    @XmlElement(name = "ActiveChargedStatusBonus")
    protected BigDecimal activeChargedStatusBonus;
    @XmlElement(name = "ExtendedAttribute")
    protected List<ExtendedAttributes> extendedAttribute;
    @XmlElement(name = "Banned")
    protected BigInteger banned;

    /**
     * Gets the value of the positionNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPositionNumber() {
        return positionNumber;
    }

    /**
     * Sets the value of the positionNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPositionNumber(BigInteger value) {
        this.positionNumber = value;
    }

    /**
     * Gets the value of the article property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArticle() {
        return article;
    }

    /**
     * Sets the value of the article property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArticle(String value) {
        this.article = value;
    }

    /**
     * Gets the value of the articleName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArticleName() {
        return articleName;
    }

    /**
     * Sets the value of the articleName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArticleName(String value) {
        this.articleName = value;
    }

    /**
     * Gets the value of the price property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * Sets the value of the price property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPrice(BigDecimal value) {
        this.price = value;
    }

    /**
     * Gets the value of the quantity property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getQuantity() {
        return quantity;
    }

    /**
     * Sets the value of the quantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setQuantity(BigDecimal value) {
        this.quantity = value;
    }

    /**
     * Gets the value of the summ property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSumm() {
        return summ;
    }

    /**
     * Sets the value of the summ property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSumm(BigDecimal value) {
        this.summ = value;
    }

    /**
     * Gets the value of the discount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDiscount() {
        return discount;
    }

    /**
     * Sets the value of the discount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDiscount(BigDecimal value) {
        this.discount = value;
    }

    /**
     * Gets the value of the summDiscounted property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSummDiscounted() {
        return summDiscounted;
    }

    /**
     * Sets the value of the summDiscounted property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSummDiscounted(BigDecimal value) {
        this.summDiscounted = value;
    }

    /**
     * Gets the value of the availablePayment property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAvailablePayment() {
        return availablePayment;
    }

    /**
     * Sets the value of the availablePayment property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAvailablePayment(BigDecimal value) {
        this.availablePayment = value;
    }

    /**
     * Gets the value of the chargedBonus property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getChargedBonus() {
        return chargedBonus;
    }

    /**
     * Sets the value of the chargedBonus property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setChargedBonus(BigDecimal value) {
        this.chargedBonus = value;
    }

    /**
     * Gets the value of the chargedStatusBonus property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getChargedStatusBonus() {
        return chargedStatusBonus;
    }

    /**
     * Sets the value of the chargedStatusBonus property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setChargedStatusBonus(BigDecimal value) {
        this.chargedStatusBonus = value;
    }

    /**
     * Gets the value of the writeoffBonus property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getWriteoffBonus() {
        return writeoffBonus;
    }

    /**
     * Sets the value of the writeoffBonus property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setWriteoffBonus(BigDecimal value) {
        this.writeoffBonus = value;
    }

    /**
     * Gets the value of the writeoffStatusBonus property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getWriteoffStatusBonus() {
        return writeoffStatusBonus;
    }

    /**
     * Sets the value of the writeoffStatusBonus property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setWriteoffStatusBonus(BigDecimal value) {
        this.writeoffStatusBonus = value;
    }

    /**
     * Gets the value of the activeChargedBonus property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getActiveChargedBonus() {
        return activeChargedBonus;
    }

    /**
     * Sets the value of the activeChargedBonus property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setActiveChargedBonus(BigDecimal value) {
        this.activeChargedBonus = value;
    }

    /**
     * Gets the value of the activeChargedStatusBonus property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getActiveChargedStatusBonus() {
        return activeChargedStatusBonus;
    }

    /**
     * Sets the value of the activeChargedStatusBonus property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setActiveChargedStatusBonus(BigDecimal value) {
        this.activeChargedStatusBonus = value;
    }

    /**
     * Gets the value of the extendedAttribute property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the extendedAttribute property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExtendedAttribute().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ExtendedAttributes }
     * 
     * 
     */
    public List<ExtendedAttributes> getExtendedAttribute() {
        if (extendedAttribute == null) {
            extendedAttribute = new ArrayList<ExtendedAttributes>();
        }
        return this.extendedAttribute;
    }

    /**
     * Gets the value of the banned property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getBanned() {
        return banned;
    }

    /**
     * Sets the value of the banned property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setBanned(BigInteger value) {
        this.banned = value;
    }

    public void setExtendedAttribute(List<ExtendedAttributes> value) {
        this.extendedAttribute = null;
        List<ExtendedAttributes> draftl = this.getExtendedAttribute();
        draftl.addAll(value);
    }

}
