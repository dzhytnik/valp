
package ru.manzanagroup.loyalty.loyalty;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Cheque complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Cheque"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://loyalty.manzanagroup.ru/loyalty.xsd}RequestBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Number" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="OperationType" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}OperationType"/&gt;
 *         &lt;element name="Payment" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}Payment" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Summ" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="Discount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="SummDiscounted" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="PaidByBonus" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="PaidByStatusBonus" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="PaidByMoney" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="CodeWord" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Coupons" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}Coupons" minOccurs="0"/&gt;
 *         &lt;element name="ExtendedAttribute" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}ExtendedAttributes" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Item" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}ChequeItem" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="ChequeType" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}ChequeType" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Cheque", propOrder = {
    "number",
    "operationType",
    "payment",
    "summ",
    "discount",
    "summDiscounted",
    "paidByBonus",
    "paidByStatusBonus",
    "paidByMoney",
    "codeWord",
    "coupons",
    "extendedAttribute",
    "item"
})
public class Cheque
    extends RequestBase
{

    @XmlElement(name = "Number")
    protected String number;
    @XmlElement(name = "OperationType", required = true)
    @XmlSchemaType(name = "string")
    protected OperationType operationType;
    @XmlElement(name = "Payment")
    protected List<Payment> payment;
    @XmlElement(name = "Summ")
    protected BigDecimal summ;
    @XmlElement(name = "Discount")
    protected BigDecimal discount;
    @XmlElement(name = "SummDiscounted")
    protected BigDecimal summDiscounted;
    @XmlElement(name = "PaidByBonus")
    protected BigDecimal paidByBonus;
    @XmlElement(name = "PaidByStatusBonus")
    protected BigDecimal paidByStatusBonus;
    @XmlElement(name = "PaidByMoney")
    protected BigDecimal paidByMoney;
    @XmlElement(name = "CodeWord")
    protected String codeWord;
    @XmlElement(name = "Coupons")
    protected Coupons coupons;
    @XmlElement(name = "ExtendedAttribute")
    protected List<ExtendedAttributes> extendedAttribute;
    @XmlElement(name = "Item")
    protected List<ChequeItem> item;
    @XmlAttribute(name = "ChequeType")
    protected ChequeType chequeType;

    /**
     * Gets the value of the number property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumber() {
        return number;
    }

    /**
     * Sets the value of the number property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumber(String value) {
        this.number = value;
    }

    /**
     * Gets the value of the operationType property.
     * 
     * @return
     *     possible object is
     *     {@link OperationType }
     *     
     */
    public OperationType getOperationType() {
        return operationType;
    }

    /**
     * Sets the value of the operationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link OperationType }
     *     
     */
    public void setOperationType(OperationType value) {
        this.operationType = value;
    }

    /**
     * Gets the value of the payment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the payment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPayment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Payment }
     * 
     * 
     */
    public List<Payment> getPayment() {
        if (payment == null) {
            payment = new ArrayList<Payment>();
        }
        return this.payment;
    }

    /**
     * Gets the value of the summ property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSumm() {
        return summ;
    }

    /**
     * Sets the value of the summ property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSumm(BigDecimal value) {
        this.summ = value;
    }

    /**
     * Gets the value of the discount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDiscount() {
        return discount;
    }

    /**
     * Sets the value of the discount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDiscount(BigDecimal value) {
        this.discount = value;
    }

    /**
     * Gets the value of the summDiscounted property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSummDiscounted() {
        return summDiscounted;
    }

    /**
     * Sets the value of the summDiscounted property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSummDiscounted(BigDecimal value) {
        this.summDiscounted = value;
    }

    /**
     * Gets the value of the paidByBonus property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPaidByBonus() {
        return paidByBonus;
    }

    /**
     * Sets the value of the paidByBonus property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPaidByBonus(BigDecimal value) {
        this.paidByBonus = value;
    }

    /**
     * Gets the value of the paidByStatusBonus property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPaidByStatusBonus() {
        return paidByStatusBonus;
    }

    /**
     * Sets the value of the paidByStatusBonus property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPaidByStatusBonus(BigDecimal value) {
        this.paidByStatusBonus = value;
    }

    /**
     * Gets the value of the paidByMoney property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPaidByMoney() {
        return paidByMoney;
    }

    /**
     * Sets the value of the paidByMoney property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPaidByMoney(BigDecimal value) {
        this.paidByMoney = value;
    }

    /**
     * Gets the value of the codeWord property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodeWord() {
        return codeWord;
    }

    /**
     * Sets the value of the codeWord property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodeWord(String value) {
        this.codeWord = value;
    }

    /**
     * Gets the value of the coupons property.
     * 
     * @return
     *     possible object is
     *     {@link Coupons }
     *     
     */
    public Coupons getCoupons() {
        return coupons;
    }

    /**
     * Sets the value of the coupons property.
     * 
     * @param value
     *     allowed object is
     *     {@link Coupons }
     *     
     */
    public void setCoupons(Coupons value) {
        this.coupons = value;
    }

    /**
     * Gets the value of the extendedAttribute property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the extendedAttribute property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExtendedAttribute().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ExtendedAttributes }
     * 
     * 
     */
    public List<ExtendedAttributes> getExtendedAttribute() {
        if (extendedAttribute == null) {
            extendedAttribute = new ArrayList<ExtendedAttributes>();
        }
        return this.extendedAttribute;
    }

    /**
     * Gets the value of the item property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the item property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChequeItem }
     * 
     * 
     */
    public List<ChequeItem> getItem() {
        if (item == null) {
            item = new ArrayList<ChequeItem>();
        }
        return this.item;
    }

    /**
     * Gets the value of the chequeType property.
     * 
     * @return
     *     possible object is
     *     {@link ChequeType }
     *     
     */
    public ChequeType getChequeType() {
        return chequeType;
    }

    /**
     * Sets the value of the chequeType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChequeType }
     *     
     */
    public void setChequeType(ChequeType value) {
        this.chequeType = value;
    }

    public void setPayment(List<Payment> value) {
        this.payment = null;
        List<Payment> draftl = this.getPayment();
        draftl.addAll(value);
    }

    public void setExtendedAttribute(List<ExtendedAttributes> value) {
        this.extendedAttribute = null;
        List<ExtendedAttributes> draftl = this.getExtendedAttribute();
        draftl.addAll(value);
    }

    public void setItem(List<ChequeItem> value) {
        this.item = null;
        List<ChequeItem> draftl = this.getItem();
        draftl.addAll(value);
    }

}
