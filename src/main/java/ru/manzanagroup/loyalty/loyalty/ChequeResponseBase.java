
package ru.manzanagroup.loyalty.loyalty;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ChequeResponseBase complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ChequeResponseBase"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://loyalty.manzanagroup.ru/loyalty.xsd}BalanceResponseBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Summ" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="Discount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="SummDiscounted" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="ChargedBonus" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="ActiveChargedBonus" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="ChargedStatusBonus" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="ActiveChargedStatusBonus" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="AvailablePayment" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="AvailableMoney" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="WriteoffBonus" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="WriteoffStatusBonus" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="Coupons" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}Coupons" minOccurs="0"/&gt;
 *         &lt;element name="InstantCoupons" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}InstantCoupons" minOccurs="0"/&gt;
 *         &lt;element name="ExtendedAttribute" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}ExtendedAttributes" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Item" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}ChequeItem" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Offer" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}Offer" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChequeResponseBase", propOrder = {
    "summ",
    "discount",
    "summDiscounted",
    "chargedBonus",
    "activeChargedBonus",
    "chargedStatusBonus",
    "activeChargedStatusBonus",
    "availablePayment",
    "availableMoney",
    "writeoffBonus",
    "writeoffStatusBonus",
    "coupons",
    "instantCoupons",
    "extendedAttribute",
    "item",
    "offer"
})
public class ChequeResponseBase
    extends BalanceResponseBase
{

    @XmlElement(name = "Summ")
    protected BigDecimal summ;
    @XmlElement(name = "Discount")
    protected BigDecimal discount;
    @XmlElement(name = "SummDiscounted")
    protected BigDecimal summDiscounted;
    @XmlElement(name = "ChargedBonus")
    protected BigDecimal chargedBonus;
    @XmlElement(name = "ActiveChargedBonus")
    protected BigDecimal activeChargedBonus;
    @XmlElement(name = "ChargedStatusBonus")
    protected BigDecimal chargedStatusBonus;
    @XmlElement(name = "ActiveChargedStatusBonus")
    protected BigDecimal activeChargedStatusBonus;
    @XmlElement(name = "AvailablePayment")
    protected BigDecimal availablePayment;
    @XmlElement(name = "AvailableMoney")
    protected BigDecimal availableMoney;
    @XmlElement(name = "WriteoffBonus")
    protected BigDecimal writeoffBonus;
    @XmlElement(name = "WriteoffStatusBonus")
    protected BigDecimal writeoffStatusBonus;
    @XmlElement(name = "Coupons")
    protected Coupons coupons;
    @XmlElement(name = "InstantCoupons")
    protected InstantCoupons instantCoupons;
    @XmlElement(name = "ExtendedAttribute")
    protected List<ExtendedAttributes> extendedAttribute;
    @XmlElement(name = "Item")
    protected List<ChequeItem> item;
    @XmlElement(name = "Offer")
    protected List<Offer> offer;

    /**
     * Gets the value of the summ property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSumm() {
        return summ;
    }

    /**
     * Sets the value of the summ property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSumm(BigDecimal value) {
        this.summ = value;
    }

    /**
     * Gets the value of the discount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDiscount() {
        return discount;
    }

    /**
     * Sets the value of the discount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDiscount(BigDecimal value) {
        this.discount = value;
    }

    /**
     * Gets the value of the summDiscounted property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSummDiscounted() {
        return summDiscounted;
    }

    /**
     * Sets the value of the summDiscounted property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSummDiscounted(BigDecimal value) {
        this.summDiscounted = value;
    }

    /**
     * Gets the value of the chargedBonus property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getChargedBonus() {
        return chargedBonus;
    }

    /**
     * Sets the value of the chargedBonus property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setChargedBonus(BigDecimal value) {
        this.chargedBonus = value;
    }

    /**
     * Gets the value of the activeChargedBonus property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getActiveChargedBonus() {
        return activeChargedBonus;
    }

    /**
     * Sets the value of the activeChargedBonus property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setActiveChargedBonus(BigDecimal value) {
        this.activeChargedBonus = value;
    }

    /**
     * Gets the value of the chargedStatusBonus property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getChargedStatusBonus() {
        return chargedStatusBonus;
    }

    /**
     * Sets the value of the chargedStatusBonus property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setChargedStatusBonus(BigDecimal value) {
        this.chargedStatusBonus = value;
    }

    /**
     * Gets the value of the activeChargedStatusBonus property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getActiveChargedStatusBonus() {
        return activeChargedStatusBonus;
    }

    /**
     * Sets the value of the activeChargedStatusBonus property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setActiveChargedStatusBonus(BigDecimal value) {
        this.activeChargedStatusBonus = value;
    }

    /**
     * Gets the value of the availablePayment property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAvailablePayment() {
        return availablePayment;
    }

    /**
     * Sets the value of the availablePayment property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAvailablePayment(BigDecimal value) {
        this.availablePayment = value;
    }

    /**
     * Gets the value of the availableMoney property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAvailableMoney() {
        return availableMoney;
    }

    /**
     * Sets the value of the availableMoney property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAvailableMoney(BigDecimal value) {
        this.availableMoney = value;
    }

    /**
     * Gets the value of the writeoffBonus property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getWriteoffBonus() {
        return writeoffBonus;
    }

    /**
     * Sets the value of the writeoffBonus property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setWriteoffBonus(BigDecimal value) {
        this.writeoffBonus = value;
    }

    /**
     * Gets the value of the writeoffStatusBonus property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getWriteoffStatusBonus() {
        return writeoffStatusBonus;
    }

    /**
     * Sets the value of the writeoffStatusBonus property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setWriteoffStatusBonus(BigDecimal value) {
        this.writeoffStatusBonus = value;
    }

    /**
     * Gets the value of the coupons property.
     * 
     * @return
     *     possible object is
     *     {@link Coupons }
     *     
     */
    public Coupons getCoupons() {
        return coupons;
    }

    /**
     * Sets the value of the coupons property.
     * 
     * @param value
     *     allowed object is
     *     {@link Coupons }
     *     
     */
    public void setCoupons(Coupons value) {
        this.coupons = value;
    }

    /**
     * Gets the value of the instantCoupons property.
     * 
     * @return
     *     possible object is
     *     {@link InstantCoupons }
     *     
     */
    public InstantCoupons getInstantCoupons() {
        return instantCoupons;
    }

    /**
     * Sets the value of the instantCoupons property.
     * 
     * @param value
     *     allowed object is
     *     {@link InstantCoupons }
     *     
     */
    public void setInstantCoupons(InstantCoupons value) {
        this.instantCoupons = value;
    }

    /**
     * Gets the value of the extendedAttribute property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the extendedAttribute property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExtendedAttribute().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ExtendedAttributes }
     * 
     * 
     */
    public List<ExtendedAttributes> getExtendedAttribute() {
        if (extendedAttribute == null) {
            extendedAttribute = new ArrayList<ExtendedAttributes>();
        }
        return this.extendedAttribute;
    }

    /**
     * Gets the value of the item property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the item property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChequeItem }
     * 
     * 
     */
    public List<ChequeItem> getItem() {
        if (item == null) {
            item = new ArrayList<ChequeItem>();
        }
        return this.item;
    }

    /**
     * Gets the value of the offer property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the offer property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOffer().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Offer }
     * 
     * 
     */
    public List<Offer> getOffer() {
        if (offer == null) {
            offer = new ArrayList<Offer>();
        }
        return this.offer;
    }

    public void setExtendedAttribute(List<ExtendedAttributes> value) {
        this.extendedAttribute = null;
        List<ExtendedAttributes> draftl = this.getExtendedAttribute();
        draftl.addAll(value);
    }

    public void setItem(List<ChequeItem> value) {
        this.item = null;
        List<ChequeItem> draftl = this.getItem();
        draftl.addAll(value);
    }

    public void setOffer(List<Offer> value) {
        this.offer = null;
        List<Offer> draftl = this.getOffer();
        draftl.addAll(value);
    }

}
