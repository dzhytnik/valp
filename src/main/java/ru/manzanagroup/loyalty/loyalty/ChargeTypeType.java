
package ru.manzanagroup.loyalty.loyalty;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ChargeType_Type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ChargeType_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Charge"/&gt;
 *     &lt;enumeration value="Write-off"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ChargeType_Type")
@XmlEnum
public enum ChargeTypeType {

    @XmlEnumValue("Charge")
    CHARGE("Charge"),
    @XmlEnumValue("Write-off")
    WRITE_OFF("Write-off");
    private final String value;

    ChargeTypeType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ChargeTypeType fromValue(String v) {
        for (ChargeTypeType c: ChargeTypeType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
