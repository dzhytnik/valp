
package ru.manzanagroup.loyalty.loyalty;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MoneyResponseBase complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MoneyResponseBase"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://loyalty.manzanagroup.ru/loyalty.xsd}ResponseBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CardMoneyDebet" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="CardMoneyCredit" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="CardMoneyBalance" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="FullMoneyBalance" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="ContactMoneyDebet" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="ContactMoneyCredit" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="ContactMoneyBalance" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="Number" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MoneyResponseBase", propOrder = {
    "cardMoneyDebet",
    "cardMoneyCredit",
    "cardMoneyBalance",
    "fullMoneyBalance",
    "contactMoneyDebet",
    "contactMoneyCredit",
    "contactMoneyBalance",
    "number"
})
public class MoneyResponseBase
    extends ResponseBase
{

    @XmlElement(name = "CardMoneyDebet")
    protected BigDecimal cardMoneyDebet;
    @XmlElement(name = "CardMoneyCredit")
    protected BigDecimal cardMoneyCredit;
    @XmlElement(name = "CardMoneyBalance")
    protected BigDecimal cardMoneyBalance;
    @XmlElement(name = "FullMoneyBalance")
    protected BigDecimal fullMoneyBalance;
    @XmlElement(name = "ContactMoneyDebet")
    protected BigDecimal contactMoneyDebet;
    @XmlElement(name = "ContactMoneyCredit")
    protected BigDecimal contactMoneyCredit;
    @XmlElement(name = "ContactMoneyBalance")
    protected BigDecimal contactMoneyBalance;
    @XmlElement(name = "Number")
    protected String number;

    /**
     * Gets the value of the cardMoneyDebet property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCardMoneyDebet() {
        return cardMoneyDebet;
    }

    /**
     * Sets the value of the cardMoneyDebet property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCardMoneyDebet(BigDecimal value) {
        this.cardMoneyDebet = value;
    }

    /**
     * Gets the value of the cardMoneyCredit property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCardMoneyCredit() {
        return cardMoneyCredit;
    }

    /**
     * Sets the value of the cardMoneyCredit property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCardMoneyCredit(BigDecimal value) {
        this.cardMoneyCredit = value;
    }

    /**
     * Gets the value of the cardMoneyBalance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCardMoneyBalance() {
        return cardMoneyBalance;
    }

    /**
     * Sets the value of the cardMoneyBalance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCardMoneyBalance(BigDecimal value) {
        this.cardMoneyBalance = value;
    }

    /**
     * Gets the value of the fullMoneyBalance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFullMoneyBalance() {
        return fullMoneyBalance;
    }

    /**
     * Sets the value of the fullMoneyBalance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFullMoneyBalance(BigDecimal value) {
        this.fullMoneyBalance = value;
    }

    /**
     * Gets the value of the contactMoneyDebet property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getContactMoneyDebet() {
        return contactMoneyDebet;
    }

    /**
     * Sets the value of the contactMoneyDebet property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setContactMoneyDebet(BigDecimal value) {
        this.contactMoneyDebet = value;
    }

    /**
     * Gets the value of the contactMoneyCredit property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getContactMoneyCredit() {
        return contactMoneyCredit;
    }

    /**
     * Sets the value of the contactMoneyCredit property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setContactMoneyCredit(BigDecimal value) {
        this.contactMoneyCredit = value;
    }

    /**
     * Gets the value of the contactMoneyBalance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getContactMoneyBalance() {
        return contactMoneyBalance;
    }

    /**
     * Sets the value of the contactMoneyBalance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setContactMoneyBalance(BigDecimal value) {
        this.contactMoneyBalance = value;
    }

    /**
     * Gets the value of the number property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumber() {
        return number;
    }

    /**
     * Sets the value of the number property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumber(String value) {
        this.number = value;
    }

}
