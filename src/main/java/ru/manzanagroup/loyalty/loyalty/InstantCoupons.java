
package ru.manzanagroup.loyalty.loyalty;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InstantCoupons complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InstantCoupons"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="InstantCoupon" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}InstantCoupon" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InstantCoupons", propOrder = {
    "instantCoupon"
})
public class InstantCoupons {

    @XmlElement(name = "InstantCoupon")
    protected List<InstantCoupon> instantCoupon;

    /**
     * Gets the value of the instantCoupon property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the instantCoupon property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInstantCoupon().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InstantCoupon }
     * 
     * 
     */
    public List<InstantCoupon> getInstantCoupon() {
        if (instantCoupon == null) {
            instantCoupon = new ArrayList<InstantCoupon>();
        }
        return this.instantCoupon;
    }

    public void setInstantCoupon(List<InstantCoupon> value) {
        this.instantCoupon = null;
        List<InstantCoupon> draftl = this.getInstantCoupon();
        draftl.addAll(value);
    }

}
