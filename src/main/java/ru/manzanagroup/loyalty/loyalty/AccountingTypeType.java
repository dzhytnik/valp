
package ru.manzanagroup.loyalty.loyalty;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AccountingType_Type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AccountingType_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Debet"/&gt;
 *     &lt;enumeration value="Credit"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "AccountingType_Type")
@XmlEnum
public enum AccountingTypeType {

    @XmlEnumValue("Debet")
    DEBET("Debet"),
    @XmlEnumValue("Credit")
    CREDIT("Credit");
    private final String value;

    AccountingTypeType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AccountingTypeType fromValue(String v) {
        for (AccountingTypeType c: AccountingTypeType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
