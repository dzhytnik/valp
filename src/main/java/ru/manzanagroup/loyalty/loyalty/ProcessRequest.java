
package ru.manzanagroup.loyalty.loyalty;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="request" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;choice maxOccurs="unbounded" minOccurs="0"&gt;
 *                     &lt;element name="BalanceRequest" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}BalanceRequestBase" minOccurs="0"/&gt;
 *                     &lt;element name="BonusRequest" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}BonusRequestBase" minOccurs="0"/&gt;
 *                     &lt;element name="CardManagementRequest" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}CardManagementRequestBase" minOccurs="0"/&gt;
 *                     &lt;element name="CardRequest" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}CardRequestBase" minOccurs="0"/&gt;
 *                     &lt;element name="ChequeRequest" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}Cheque" minOccurs="0"/&gt;
 *                     &lt;element name="MoneyRequest" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}Money" minOccurs="0"/&gt;
 *                     &lt;element name="MoneyRollback" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}MoneyRollbackBase" minOccurs="0"/&gt;
 *                     &lt;element name="OfferRequest" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}Cheque" minOccurs="0"/&gt;
 *                   &lt;/choice&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="orgName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "request",
    "orgName"
})
@XmlRootElement(name = "ProcessRequest")
public class ProcessRequest {

    protected ProcessRequest.Request request;
    protected String orgName;

    /**
     * Gets the value of the request property.
     * 
     * @return
     *     possible object is
     *     {@link ProcessRequest.Request }
     *     
     */
    public ProcessRequest.Request getRequest() {
        return request;
    }

    /**
     * Sets the value of the request property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessRequest.Request }
     *     
     */
    public void setRequest(ProcessRequest.Request value) {
        this.request = value;
    }

    /**
     * Gets the value of the orgName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrgName() {
        return orgName;
    }

    /**
     * Sets the value of the orgName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrgName(String value) {
        this.orgName = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;choice maxOccurs="unbounded" minOccurs="0"&gt;
     *           &lt;element name="BalanceRequest" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}BalanceRequestBase" minOccurs="0"/&gt;
     *           &lt;element name="BonusRequest" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}BonusRequestBase" minOccurs="0"/&gt;
     *           &lt;element name="CardManagementRequest" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}CardManagementRequestBase" minOccurs="0"/&gt;
     *           &lt;element name="CardRequest" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}CardRequestBase" minOccurs="0"/&gt;
     *           &lt;element name="ChequeRequest" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}Cheque" minOccurs="0"/&gt;
     *           &lt;element name="MoneyRequest" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}Money" minOccurs="0"/&gt;
     *           &lt;element name="MoneyRollback" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}MoneyRollbackBase" minOccurs="0"/&gt;
     *           &lt;element name="OfferRequest" type="{http://loyalty.manzanagroup.ru/loyalty.xsd}Cheque" minOccurs="0"/&gt;
     *         &lt;/choice&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "balanceRequestOrBonusRequestOrCardManagementRequest"
    })
    public static class Request {

        @XmlElementRefs({
            @XmlElementRef(name = "BalanceRequest", namespace = "http://loyalty.manzanagroup.ru/loyalty.xsd", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "BonusRequest", namespace = "http://loyalty.manzanagroup.ru/loyalty.xsd", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "CardManagementRequest", namespace = "http://loyalty.manzanagroup.ru/loyalty.xsd", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "CardRequest", namespace = "http://loyalty.manzanagroup.ru/loyalty.xsd", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "ChequeRequest", namespace = "http://loyalty.manzanagroup.ru/loyalty.xsd", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "MoneyRequest", namespace = "http://loyalty.manzanagroup.ru/loyalty.xsd", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "MoneyRollback", namespace = "http://loyalty.manzanagroup.ru/loyalty.xsd", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "OfferRequest", namespace = "http://loyalty.manzanagroup.ru/loyalty.xsd", type = JAXBElement.class, required = false)
        })
        protected List<JAXBElement<? extends RequestBase>> balanceRequestOrBonusRequestOrCardManagementRequest;

        /**
         * Gets the value of the balanceRequestOrBonusRequestOrCardManagementRequest property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the balanceRequestOrBonusRequestOrCardManagementRequest property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getBalanceRequestOrBonusRequestOrCardManagementRequest().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link JAXBElement }{@code <}{@link BalanceRequestBase }{@code >}
         * {@link JAXBElement }{@code <}{@link BonusRequestBase }{@code >}
         * {@link JAXBElement }{@code <}{@link CardManagementRequestBase }{@code >}
         * {@link JAXBElement }{@code <}{@link CardRequestBase }{@code >}
         * {@link JAXBElement }{@code <}{@link Cheque }{@code >}
         * {@link JAXBElement }{@code <}{@link Money }{@code >}
         * {@link JAXBElement }{@code <}{@link MoneyRollbackBase }{@code >}
         * {@link JAXBElement }{@code <}{@link Cheque }{@code >}
         * 
         * 
         */
        public List<JAXBElement<? extends RequestBase>> getBalanceRequestOrBonusRequestOrCardManagementRequest() {
            if (balanceRequestOrBonusRequestOrCardManagementRequest == null) {
                balanceRequestOrBonusRequestOrCardManagementRequest = new ArrayList<JAXBElement<? extends RequestBase>>();
            }
            return this.balanceRequestOrBonusRequestOrCardManagementRequest;
        }

        public void setBalanceRequestOrBonusRequestOrCardManagementRequest(List<JAXBElement<? extends RequestBase>> value) {
            this.balanceRequestOrBonusRequestOrCardManagementRequest = null;
            List<JAXBElement<? extends RequestBase>> draftl = this.getBalanceRequestOrBonusRequestOrCardManagementRequest();
            draftl.addAll(value);
        }

    }

}
