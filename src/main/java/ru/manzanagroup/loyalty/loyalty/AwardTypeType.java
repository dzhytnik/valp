
package ru.manzanagroup.loyalty.loyalty;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AwardType_Type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AwardType_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Bonus"/&gt;
 *     &lt;enumeration value="Discount"/&gt;
 *     &lt;enumeration value="Money"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "AwardType_Type")
@XmlEnum
public enum AwardTypeType {

    @XmlEnumValue("Bonus")
    BONUS("Bonus"),
    @XmlEnumValue("Discount")
    DISCOUNT("Discount"),
    @XmlEnumValue("Money")
    MONEY("Money");
    private final String value;

    AwardTypeType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AwardTypeType fromValue(String v) {
        for (AwardTypeType c: AwardTypeType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
