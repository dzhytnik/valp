
package ru.manzanagroup.loyalty.loyalty;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ActionTime_Type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ActionTime_Type"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Immediately"/&gt;
 *     &lt;enumeration value="At time"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ActionTime_Type")
@XmlEnum
public enum ActionTimeType {

    @XmlEnumValue("Immediately")
    IMMEDIATELY("Immediately"),
    @XmlEnumValue("At time")
    AT_TIME("At time");
    private final String value;

    ActionTimeType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ActionTimeType fromValue(String v) {
        for (ActionTimeType c: ActionTimeType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
