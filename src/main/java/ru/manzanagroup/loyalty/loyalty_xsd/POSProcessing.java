package ru.manzanagroup.loyalty.loyalty_xsd;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;

/**
 * Webservice для обработки запросов от POS терминалов.
 *
 * This class was generated by Apache CXF 3.3.4
 * 2019-11-27T15:30:26.030+02:00
 * Generated source version: 3.3.4
 *
 */
@WebServiceClient(name = "POSProcessing",
                  wsdlLocation = "WEB-INF/wsdl/POSProcessing.wsdl",
                  targetNamespace = "http://loyalty.manzanagroup.ru/loyalty.xsd")
public class POSProcessing extends Service {

    public final static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("http://loyalty.manzanagroup.ru/loyalty.xsd", "POSProcessing");
    public final static QName POSProcessingSoap = new QName("http://loyalty.manzanagroup.ru/loyalty.xsd", "POSProcessingSoap");
    public final static QName POSProcessingSoap12 = new QName("http://loyalty.manzanagroup.ru/loyalty.xsd", "POSProcessingSoap12");
    static {
        URL url = null;
        try {
            url = new URL("file:/C:/Users/dmoroz/IdeaProjects/valp/src/main/resources/POSProcessing.wsdl");
        } catch (MalformedURLException e) {
            java.util.logging.Logger.getLogger(POSProcessing.class.getName())
                .log(java.util.logging.Level.INFO,
                     "Can not initialize the default wsdl from {0}", "file:/C:/Users/dmoroz/IdeaProjects/valp/src/main/resources/POSProcessing.wsdl");
        }
        WSDL_LOCATION = url;
    }

    public POSProcessing(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public POSProcessing(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public POSProcessing() {
        super(WSDL_LOCATION, SERVICE);
    }

    public POSProcessing(WebServiceFeature ... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }

    public POSProcessing(URL wsdlLocation, WebServiceFeature ... features) {
        super(wsdlLocation, SERVICE, features);
    }

    public POSProcessing(URL wsdlLocation, QName serviceName, WebServiceFeature ... features) {
        super(wsdlLocation, serviceName, features);
    }




    /**
     *
     * @return
     *     returns POSProcessingSoap
     */
    @WebEndpoint(name = "POSProcessingSoap")
    public POSProcessingSoap getPOSProcessingSoap() {
        return super.getPort(POSProcessingSoap, POSProcessingSoap.class);
    }

    /**
     *
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns POSProcessingSoap
     */
    @WebEndpoint(name = "POSProcessingSoap")
    public POSProcessingSoap getPOSProcessingSoap(WebServiceFeature... features) {
        return super.getPort(POSProcessingSoap, POSProcessingSoap.class, features);
    }


    /**
     *
     * @return
     *     returns POSProcessingSoap
     */
    @WebEndpoint(name = "POSProcessingSoap12")
    public POSProcessingSoap getPOSProcessingSoap12() {
        return super.getPort(POSProcessingSoap12, POSProcessingSoap.class);
    }

    /**
     *
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns POSProcessingSoap
     */
    @WebEndpoint(name = "POSProcessingSoap12")
    public POSProcessingSoap getPOSProcessingSoap12(WebServiceFeature... features) {
        return super.getPort(POSProcessingSoap12, POSProcessingSoap.class, features);
    }

}
