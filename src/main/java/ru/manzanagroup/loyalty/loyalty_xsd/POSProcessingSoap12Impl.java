
/**
 * Please modify this class to meet your needs
 * This class is not complete
 */

package ru.manzanagroup.loyalty.loyalty_xsd;

import ru.manzanagroup.loyalty.loyalty.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;


/**
 * This class was generated by Apache CXF 3.3.4
 * 2019-11-27T15:30:26.013+02:00
 * Generated source version: 3.3.4
 *
 */

@javax.jws.WebService(
                      serviceName = "POSProcessing",
                      portName = "POSProcessingSoap12",
                      targetNamespace = "http://loyalty.manzanagroup.ru/loyalty.xsd",
                      wsdlLocation = "WEB-INF/wsdl/POSProcessing.wsdl",
                      endpointInterface = "ru.manzanagroup.loyalty.loyalty_xsd.POSProcessingSoap")

public class POSProcessingSoap12Impl implements POSProcessingSoap {

    private static final Logger LOG = Logger.getLogger(POSProcessingSoap12Impl.class.getName());

    /* (non-Javadoc)
     * @see ru.manzanagroup.loyalty.loyalty_xsd.POSProcessingSoap#processRequest(ru.manzanagroup.loyalty.loyalty.ProcessRequest.Request request, java.lang.String orgName)*
     */
    public ru.manzanagroup.loyalty.loyalty.ProcessRequestResponse.ProcessRequestResult processRequest(ru.manzanagroup.loyalty.loyalty.ProcessRequest.Request request, java.lang.String orgName) {
        LOG.info("Executing operation processRequest");
        System.out.println(request);
        System.out.println(orgName);
        try {
            ru.manzanagroup.loyalty.loyalty.ProcessRequestResponse.ProcessRequestResult result = new ProcessRequestResponse.ProcessRequestResult();
            ChequeResponseBase chequeResponseBase = new ChequeResponseBase();
            chequeResponseBase.setMessage("Доступных баллов недостаточно для оплаты");
            chequeResponseBase.setCardChargedMoney(BigDecimal.ZERO);
            chequeResponseBase.setCardWriteoffMoney(BigDecimal.ZERO);
            chequeResponseBase.setCardMoneyBalance(BigDecimal.ZERO);
            chequeResponseBase.setAvailableMoney(new BigDecimal("0.0"));
            chequeResponseBase.setCardChargedMoney(BigDecimal.ZERO);
            List<ResponseBase> responseElements = new ArrayList<ResponseBase>();
            responseElements.add(chequeResponseBase);
            result.setBalanceResponseOrBonusResponseOrCardManagementResponse(responseElements);
            return result;
        } catch (java.lang.Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
    }

}
